# from the cpu shell
Intel Pentium
Icomp 2 index: 127
id: A80502166
S-Spec: SY016
Shell year: 1992 1993

https://www.cpu-world.com/sspec/SY/SY016.html
# from cpu-world
Socket: Socket 7 Socket 5
CPU Family: Pentium  
Introduction date: January 4, 1996 
Microarchitecture: P5
Processor core: P54CS
Process: 0.35 micron
Core stepping: cC0
CPUID: 52C
Family: 5 (05h)
Model: 2 (02h)
Stepping: 12 (0Ch)
Core voltage: 3.3 volts
Speed: 166 mhz
FSB: 66 mhz
TDP:  14.5 watts