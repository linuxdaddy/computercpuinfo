Created by Jeff Cataract

Photos in this directory are taken by me and released under the following license
and can be used freely.

CC BY

https://creativecommons.org/licenses/by/4.0

This license lets others distribute, remix, adapt, and build upon your work, even commercially, as long as they credit you for the original creation.