# from the cpu shell
Intel Pentium 4
id: ?
Speed: 2A Ghz
FSB: 400 mhz
Cache: 512 KB 
S-Spec: SL5YR
Country of origin: Costa Rica
Core voltage: 1.5 volts
Shell year: 2001

https://www.cpu-world.com/sspec/SL/SL5RY.html
# from cpu-world
Socket: Socket 478
Process: 0.13 micron CMOS technology
CPU Family: Intel Pentium 4  
Introduction date: January 7, 2002
Introduction price: $364
Microarchitecture: Netburst
Processor core: Northwood
Core stepping: B0
CPUID: F24
Family: 15 (0Fh)
Model: 2 (02h)
Stepping: 4 (04h)
TDP: 52.4 watts

